import React from 'react';
import { connect } from 'react-redux';
import { FaChartLine, FaChartBar, FaCartPlus } from 'react-icons/fa'
import { MdEvent } from "react-icons/md";
import { TiGroupOutline } from "react-icons/ti";
import { Link } from 'react-router-dom';

const Home = props => (
    <div id="wrapper">
        <div className="wrapper-header">
            <div className="col-lg">
                <h1 className="page-header">
                    Bienvenido!
                </h1>
            </div>
        </div>
        <div className="wrapper-body">
            <div className="row border">
                <h2>Estadísticas</h2>
                <div class="col-md-3">
                    <div class="square-service-block">
                        <Link to="/contadores">
                            <div class="ssb-icon"><FaChartBar /></div>
                            <h2 class="ssb-title">Contadores</h2>
                        </Link>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="square-service-block">
                        <Link to="/tendencias">
                            <div class="ssb-icon"><FaChartLine /></div>
                            <h2 class="ssb-title">Tendencias</h2>
                        </Link>
                    </div>
                </div>
            </div>
            <div className="row border">
                <h2>Apps</h2>

                <div class="col-md-3">
                    <div class="square-service-block">
                        <a href="http://events.supermarket-dssd.ml/" target="_blank">
                            <div class="ssb-icon"><MdEvent /></div>
                            <h2 class="ssb-title">Eventos</h2>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="square-service-block">
                        <a href="http://bpm.supermarket-dssd.ml/bonita" target="_blank">
                            <div class="ssb-icon"><TiGroupOutline /></div>
                            <h2 class="ssb-title">Bonita</h2>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="square-service-block">
                        <a href="http://web.supermarket-dssd.ml/" target="_blank">
                            <div class="ssb-icon"><FaCartPlus /></div>
                            <h2 class="ssb-title">Web Ventas</h2>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
);

const mapStateToProps = (state, ownProps) => {
    return state;
}

export default connect(mapStateToProps)(Home);
