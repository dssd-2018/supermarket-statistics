﻿import React from 'react';
import { Link, NavLink as NavLinkRouter } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import { NavbarBrand, Navbar, NavbarToggler, Collapse, Nav, NavLink, NavItem } from 'reactstrap';
import {  MdAccessibility, MdVpnKey, MdPieChart } from 'react-icons/md'
import { FaDatabase, FaChartBar } from 'react-icons/fa'
//import './NavMenu.css';}


export default class extends React.Component {
    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {

        return (
            <Navbar sticky="top" className="navbar-expand-md navbar-dark bg-dark">
                <NavbarBrand tag={Link} to={'/'}>
                   Estadísticas
                </NavbarBrand>
                <NavbarToggler onClick={this.toggleNavbar} />
                <Collapse isOpen={!this.state.collapsed} navbar>
                    <Nav navbar>
                        <LinkContainer to={'/contadores'} exact>
                            <NavItem>
                                <NavLink>
                                    <FaChartBar /> Contadores
                                </NavLink>
                            </NavItem>
                        </LinkContainer>
                    </Nav>

                    <Nav navbar>
                        <LinkContainer to={'/tendencias'} exact>
                            <NavItem>
                                <NavLink>
                                    <MdPieChart /> Tendencias
                                </NavLink>
                            </NavItem>
                        </LinkContainer>
                    </Nav>

                    <Nav className="ml-auto" navbar>
                        <LinkContainer to={'/logout'} exact>
                            <NavItem>
                                <NavLink>
                                    <MdVpnKey /> Cerrar Sesión
                                </NavLink>
                            </NavItem>
                        </LinkContainer>
                    </Nav>

                </Collapse>
            </Navbar>
        )
    }
}
