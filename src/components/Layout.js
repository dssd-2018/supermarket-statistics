import React from 'react';
import { Container } from 'reactstrap';
import NavMenu from './NavMenu';

export default props => (
    <React.Fragment>
        <NavMenu />
        <Container fluid>
            {props.children}
        </Container>
    </React.Fragment>
);
