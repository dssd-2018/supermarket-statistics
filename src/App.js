﻿import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import { ToastContainer } from 'react-toastify';

import LoginPage from './modules/auth/containers/LoginPage';
import LogoutPage from './modules/auth/containers/LogoutPage';
import CountersPage from './modules/counters/container'
import TrendsPage from './modules/trends/container'


const Private = (props) => {
    if (localStorage.getItem('JWT_LOGIN')) {
        return (<React.Fragment> {props.children} </React.Fragment>)
    } else {
        return (<React.Fragment><LoginPage /></React.Fragment>)
    }
}

export default () => (
    <div>
        <Private>
            <Layout>
                <Route exact path='/' component={Home} />
                <Route path='/Contadores' component={CountersPage} />
                <Route path='/tendencias' component={TrendsPage} />

                <Route path="/logout" component={LogoutPage} />
            </Layout>
            <ToastContainer autoClose={2000} />
        </Private>
    </div>
);
