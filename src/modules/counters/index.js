import { goBack as goBackRedux } from 'react-router-redux'
import api from '../common/api'
import * as _ from 'lodash';
import { toast } from 'react-toastify'

export const COUNTERS_REQUEST = 'COUNTERS/REQUEST'
export const COUNTERS_RESPONSE = 'COUNTERS/RESPONSE'
export const COUNTERS_ERROR = 'COUNTERS/ERROR'

let initialState = {
    data: {
        sum_total: 0,
        items_total: 0,
        max_producto: { quantity: 0, items: [] },
        sales: []
    },
    loading: true,
    error: null
}

export default function reducer(state = initialState, action = {}) {
    let diff;
    switch (action.type) {
        case COUNTERS_REQUEST:
            return { ...state, loading: true }
        case COUNTERS_RESPONSE:
            let data = Object.keys(action.payload).length > 0 ? action.payload : data.initialState;
            return { ...state, loading: false, data: data };
        case COUNTERS_ERROR:
            return { ...state, loading: false, error: action.error, data: initialState.data }
        default:
            return state;
    }
}

export const load = () => (dispatch) => {

    dispatch({ type: COUNTERS_REQUEST })
    const url = "cards"
    api.post(url)
        .then((response) => {
            if (response.status == 200) {
                dispatch({ type: COUNTERS_RESPONSE, payload: response.data })
            } else {
                dispatch({ type: COUNTERS_ERROR, error: "error" })
            }
        })
        .catch((error) => {
            toast.error("Ocurrió un error")
            dispatch({ type: COUNTERS_ERROR, error: "error" })
        })
}

export const goBack = (id) => (dispatch, state) => {
    dispatch(goBackRedux())
}