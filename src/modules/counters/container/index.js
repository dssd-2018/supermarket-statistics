import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Spinner from '../../common/loading/spinner'
import Counters from '../presentational'
import { load, goBack } from '../index';

class Page extends React.Component {

    componentWillMount() {
        this.props.load()
    }

    render() {
        return (
            <Spinner loading={this.props.loading}>
                <Counters {...this.props} />
            </Spinner>
        )
    }
}

const mapStateToProps = ({ counter }) => {
    return ({
        data: counter.data,
        loading: counter.loading,
        error: counter.error,
    })
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ load, goBack }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Page)