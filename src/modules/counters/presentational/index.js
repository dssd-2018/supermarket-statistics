import React from "react";
import _ from 'lodash';
import moment from 'moment'
import './styles.css';

import {
    Body, Header, Wrapper
} from '../../common/page';
import { FaCheck } from 'react-icons/fa'

const Counters = (
    {
        data: {
            sum_total,
            items_total,
            max_producto,
            sales
        },
        load
    }
) =>
    <Wrapper>
        <Header title="Contadores Generales"></Header>
        <Body>
            <div className="container">
                <div className="row">
                    <button className="btn btn-sm btn-primary" onClick={() => load()}>Reload</button>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="card-counter primary">
                            <i className="fa fa-code-fork"></i>
                            <span className="count-numbers">$ {parseFloat(sum_total).toFixed(2)}</span>
                            <span className="count-name">Ingresos Totales</span>
                        </div>
                    </div>

                    <div className="col-md-4">
                        <div className="card-counter danger">
                            <i className="fa fa-ticket"></i>
                            <span className="count-numbers">{items_total}</span>
                            <span className="count-name">total items vendidos</span>
                        </div>
                    </div>

                    {max_producto.items && max_producto.items.map((producto, i) => (
                        <div key={i} className="col-md-4">
                            <div className="card-counter success">
                                <i className="fa fa-database"></i>
                                <span className="count-numbers">{producto.quantity} item</span>
                                <span className="count-name">{producto.product.name} más vendido</span>
                            </div>
                        </div>))
                    }
                </div>
                <div className="row">
                    <table className="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Fecha</th>
                                <th scope="col-sm">Empleado</th>
                                <th scope="col">Item</th>
                                <th scope="col">Precio Unitario</th>
                                <th scope="col">Unidades</th>
                                <th scope="col">Descuento</th>
                                <th scope="col">Total S/D</th>
                                <th scope="col">Final</th>
                                <th scope="col">Cupón</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                sales.map((s, i) => (
                                    <tr key={i + 1}>
                                        <td>{moment(s.saleDate).format("DD/MM/YYYY")}</td>
                                        <td style={{ wordWrap: 'break-word' }}>{s.employee && s.employee.username}</td>

                                        {
                                            s.items.map((x, i) => (
                                                <React.Fragment key={i}>
                                                    <td><span class="text-primary">{x.product.name}</span></td>
                                                    <td><span className="text-success"> ${(x.price / x.quantity).toFixed(2)} </span></td>
                                                    <td> <span className="text-info">{x.quantity} (items) </span></td>
                                                    <td><span className="text-danger">- ${x.discount.toFixed(2)} </span></td>
                                                </React.Fragment>
                                            ))
                                        }

                                        <td>$ {_.reduce(s.items.map(x => x.price + x.discount), (a, b) => a + b, 0).toFixed(2)}</td>
                                        <td>$ {_.reduce(s.items.map(x => x.price), (a, b) => a + b, 0).toFixed(2)}</td>
                                        <td>
                                            {
                                                Object.keys(s.coupon).length > 0
                                                    ? <FaCheck />
                                                    : ''
                                            }
                                        </td>
                                    </tr>))
                            }

                        </tbody>
                    </table></div>
            </div>
        </Body>
    </Wrapper>



export default Counters;