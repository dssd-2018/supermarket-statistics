import { goBack as goBackRedux } from 'react-router-redux'
import api from '../common/api'
import * as _ from 'lodash';
import { toast } from 'react-toastify'
import moment from 'moment';

export const TRENDS_REQUEST = 'TRENDS/REQUEST'
export const TRENDS_RESPONSE = 'TRENDS/RESPONSE'
export const TRENDS_ERROR = 'TRENDS/ERROR'

let initialState = {
    data: {
        sum_total: 0,
        items_total: 0,
        max_producto: { quantity: 0, items: [] },
        sales: [],
        products: []
    },
    loading: true,
    error: null
}

export default function reducer(state = initialState, action = {}) {
    let diff;
    switch (action.type) {
        case TRENDS_REQUEST:
            return { ...state, loading: true }
        case TRENDS_RESPONSE:
            let data = Object.keys(action.payload).length > 0 ? action.payload : data.initialState;
            return { ...state, loading: false, data: data };
        case TRENDS_ERROR:
            return { ...state, loading: false, error: action.error, data: initialState.data }
        default:
            return state;
    }
}

export const load = () => (dispatch) => {

    dispatch({ type: TRENDS_REQUEST })
    const url = "cards"
    api.post(url)
        .then((response) => {
            if (response.status == 200) {

                dispatch({
                    type: TRENDS_RESPONSE,
                    payload: response.data,
                });

            } else {
                dispatch({ type: TRENDS_ERROR, error: "error" })
            }
        })
        .catch((error) => {
            toast.error("Ocurrió un error")
            dispatch({ type: TRENDS_ERROR, error: "error" })
        })
}

export const goBack = (id) => (dispatch, state) => {
    dispatch(goBackRedux())
}