import React from "react";
import _ from 'lodash';
import moment from 'moment'

import {
    Body, Header, Wrapper
} from '../../common/page';

import Chart from "./ProductsChart";

const Counters = (
    {
        data: {
            sum_total,
            items_total,
            max_producto,
            sales,
            products
        },
        load,
        ...props
    }
) =>
    <Wrapper>
        <Header title="Tendencias"></Header>
        <Body>
            <div className="container">
                <div className="row">
                    <button className="btn btn-sm btn-primary" onClick={() => load()}>Reload</button>
                </div>
                <div className="row">
                    <Chart products={products} />
                </div>
            </div>
        </Body>
    </Wrapper>



export default Counters;