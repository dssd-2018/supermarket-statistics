import React from 'react';
import { PieChart, Pie, Legend, Tooltip } from 'recharts'

const data01 = [{ name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
{ name: 'Group C', value: 300 }, { name: 'Group D', value: 200 },
{ name: 'Group E', value: 278 }, { name: 'Group F', value: 189 }]

const ProductsChart = ({
    products
}) => {
    return (
        <PieChart width={800} height={400}>
            <Pie
                isAnimationActive={false}
                data={products.map(p => ({ name: p.product.name, value: p.quantity }))}
                cx={150}
                cy={150}
                outerRadius={100}
                fill="#8884d8"
                label
                legendType='diamond'
                />
            {/* <Pie data={data02} cx={500} cy={200} innerRadius={40} outerRadius={80} fill="#82ca9d" /> */}
            <Tooltip />
        </PieChart>
    );
}

export default ProductsChart;