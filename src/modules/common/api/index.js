import axios from 'axios';

const api = axios.create({
    baseURL: process.env.GATEWAY_URL || "http://gateway.supermarket-dssd.ml/bdm" // http://localhost:3001/bdm"
});

api.interceptors.request.use(config => {
    // if (localStorage.getItem('JWT_LOGIN')) {
    //     config.headers.common.Authorization = `Bearer ${localStorage.getItem('JWT_LOGIN')}`;
    // }

    return config;
})

export default api;
